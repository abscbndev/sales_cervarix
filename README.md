ABS-CBN Sales - Cervarix - Glaxo Smith Klein

Cervarix is a vaccine against certain types of cancer-causing human papillomavirus (HPV).

Cervarix is designed to prevent infection from HPV types 16 and 18, that cause about 70% of cervical cancer cases. These types also cause most HPV-induced genital and head and neck cancers. Additionally, some cross-reactive protection against virus strains 45 and 31 were shown in clinical trials.[2] Cervarix also contains AS04, a proprietary adjuvant that has been found to boost the immune system response for a longer period of time.

Cervarix is manufactured by GlaxoSmithKline. An alternative product, from Merck & Co., is known as Gardasil.

ABS-CBN and Cervarix are teaming up to create awareness for Cervical Cancer. The microsite aims to promote awareness via
photo uploading, branded with Cervarix, and shared to social networks.